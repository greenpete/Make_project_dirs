#!/bin/bash

# Figure out where the root folder of this script is on the users computer...
current_dir=$(pwd)

# Checking to see if there is a 'bin' directory in the users home directory and if not, we will create it.
echo "Looking to see if you have the ~/bin directory"
if [ -d "$HOME"/bin ];
  then echo "$HOME/bin exists, nothing to do..."
    else echo "Nope! So we will create it now! " && mkdir "$HOME"/bin
fi

# Finally copy the project directory over to the final place - /home/user/bin/...
echo "Copying files..."
cp -r "$current_dir" "$HOME"/bin
echo "Done!"

# Edit the desktop shortcut...
sed -i "s|Exec=|Exec=$HOME/bin/Make_Project_Dirs/Make_Project_Dirs.sh|" "$HOME"/bin/Make_Project_Dirs/Make_Project_Dirs.desktop
sed -i "s|Icon=|Icon=$HOME/bin/Make_Project_Dirs/icon.svg|" "$HOME"/bin/Make_Project_Dirs/Make_Project_Dirs.desktop
# Copy menu file...
echo "Installing shortcut..."
mv "$HOME"/bin/Make_Project_Dirs/Make_Project_Dirs.desktop "$HOME"/.local/share/applications
echo "Done!"

# Clean up...
echo "Cleaning up..."
rm "$HOME"/bin/Make_Project_Dirs/install.sh
# rm -rf "$HOME"/bin/Make_Project_Dirs/.idea # Remove if not needed.
echo "Done!"

# Let the user know we're done!
echo "
      All done! Press 'Enter' to close this window"
read -r
