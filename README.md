## Make Project Dirs

A bash script to make a directory structure for graphics (and other) projects in Linux systems, that includes template files for the more common projects, (at least the projects that I work on. You can change these if needed).

## Ease the workflow of projects.

Having a structure of directories with blank templates in relevant directories is a great way to get started on a project. It helps with organising and catagorising your assets.

If you use a good directory structure for every project, the job is easier and you get comfortable and used to the work space, which speeds up the work flow.

## Installing the script

Run install.sh - simples!

## Running the Script

You will find a shortcut in your menu under 'Programming', click on that and it will ask you for the name of the project you are about to start. Hit enter, and you will create a directory on your desktop populated with directories and file templates.
You can them move the directory to where ever you like once you have either created it, or finished the project.

## Customising the script

You can add or remove directories or templates by editing the Make_Project_Dirs.sh file and/or the Assets directory, though you will need to understand bash at least a little to do that.

## Contributing

Please feel feel free to raise an issue or submit a pull request if you think this script could be improved.