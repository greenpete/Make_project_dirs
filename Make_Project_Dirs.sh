#!/bin/bash
# Script to create directory structures for multimedia projects.
# By Peter Green
# Website - https://greenpete.co.uk/
# Version - 2.4
# Dates - Started developing - 16-09-2018 | Last modified - 02-01-2023
# Licence - MIT

cd ~/Desktop || exit
    echo "




                      #################################
                      #                               #
                      #  NEW MULTIMEDIA PROJECT! :-)  # 
                      #                               #
                      #################################

  This script will create your new directory structure on your Desktop.
  It will include an Inkscape and Scribus template too.


  What would you like to call your new project?"
read -r project_name
    mkdir "$project_name"
cd "$project_name" || exit
    mkdir -p SRC/Audacity SRC/Ardour SRC/Blender SRC/Inkscape SRC/GIMP SRC/Scribus Export SRC/Kdenlive SRC/SH3D SRC/Krita Assets/Inspiration Assets/Img Assets/Audio Assets/Rushes Assets/Docs

#Copy template files into relevant dirs...
cd ~/bin/Make_Project_Dirs/Assets || exit

    cp scribus.sla ~/Desktop/"$project_name"/SRC/Scribus
    cp inkscape.svg ~/Desktop/"$project_name"/SRC/Inkscape
    cp kdenlive.kdenlive ~/Desktop/"$project_name"/SRC/Kdenlive

#Rename them to match the project name...
mv ~/Desktop/"$project_name"/SRC/Scribus/scribus.sla ~/Desktop/"$project_name"/SRC/Scribus/"$project_name".sla
mv ~/Desktop/"$project_name"/SRC/Inkscape/inkscape.svg ~/Desktop/"$project_name"/SRC/Inkscape/"$project_name".svg
mv ~/Desktop/"$project_name"/SRC/Kdenlive/kdenlive.kdenlive ~/Desktop/"$project_name"/SRC/Kdenlive/"$project_name".kdenlive

clear

echo "





                Your project folder, '$project_name' has been created.
                
                       You will find it on your Desktop!

                #############################################
                #                                           #
                #  Please press enter to close this window. #
                #                                           #
                #############################################"

# Now lets empty the variable to clear space in RAM.
# Not sure if that's needed but it seem like a polite thing to do!
unset "$project_name"
read -r
